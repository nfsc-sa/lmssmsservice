﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Timers;
using NLog;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Http;
//using System.Net;


namespace LMSSMSService
{
	public partial class Service : ServiceBase
	{
		private System.Timers.Timer timer1;
		private string timeString;
		public int getCallType;
		private System.Timers.Timer objTimer;
		private bool isSynchronizing = false;

		private const string EventSource = "SMS Notification Service";
		private const string ApplicationEventLog = "Application";
		private const string RequestType = "SMSSubmitReq";
		private const string Error = "Error";
		private const string Success = "+ok";
		bool IsServiceEnable = true;
		private string defaultSenderName = ConfigurationManager.AppSettings["SMSSenderName"].ToString();
		int batchLimit = 10;
		Logger logger = LogManager.GetCurrentClassLogger();
		string APIurl = string.Empty;
		string user = string.Empty;
		string pass = string.Empty;
		string userId = string.Empty;

		/////////////////////////////////////////////////////////////////////
		public Service()
		{
			InitializeComponent();

		}
		public void OnDebug()
		{
			OnStart(null);
		}
		/////////////////////////////////////////////////////////////////////
		protected override void OnStart(string[] args)
		{
			int strTime = Convert.ToInt32(ConfigurationManager.AppSettings["callDuration"]);
			getCallType = Convert.ToInt32(ConfigurationManager.AppSettings["CallType"]);
			if (getCallType == 1)
			{
				timer1 = new System.Timers.Timer();
				double inter = strTime * 1000;// (double)GetNextInterval();
				timer1.Interval = inter;
				timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
				//ActivateService();
			}
			else
			{
				timer1 = new System.Timers.Timer();
				timer1.Interval = strTime * 1000;
				timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
			}
			timer1.AutoReset = true;
			timer1.Enabled = true;
			//SendMailService.WriteErrorLog("Service started");
			logger.Info("Service Started");
		}

		/////////////////////////////////////////////////////////////////////
		protected override void OnStop()
		{
			timer1.AutoReset = false;
			timer1.Enabled = false;
			//SendMailService.WriteErrorLog("Service stopped");
			AppendLog("Service stopped");

		}

		/////////////////////////////////////////////////////////////////////
		private double GetNextInterval()
		{
			timeString = ConfigurationManager.AppSettings["StartTime"];
			DateTime t = DateTime.Parse(timeString);
			TimeSpan ts = new TimeSpan();
			int x;
			ts = t - System.DateTime.Now;
			if (ts.TotalMilliseconds < 0)
			{
				ts = t.AddDays(1) - System.DateTime.Now;//Here you can increase the timer interval based on your requirments.   
			}
			return ts.TotalMilliseconds;
		}

		/////////////////////////////////////////////////////////////////////
		private void SetTimer()
		{
			try
			{
				double inter = (double)GetNextInterval();
				timer1.Interval = inter;
				timer1.Start();
			}
			catch (Exception ex)
			{
			}
		}

		/////////////////////////////////////////////////////////////////////
		private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
		{

			try
			{
				timer1.Stop();
				///Activates Service to Send SMS from DB
				ActivateService();
			}
			catch (Exception ex)
			{
				isSynchronizing = false;

				SMSService.WriteErrorLog(ex);
			}
            finally
            {
				timer1.Start();

			}
		}
		public void AppendLog(string log)
		{
			//SMSService.AppendLog(log);
			logger.Info(log);

		}
		/// <summary>
		/// Execute the code for send SMS from SMSTracking table
		/// </summary>
		public void ActivateService()
		{
			
			StringBuilder sbLog = new StringBuilder();
			try
			{
				AppendLog("Activated the service...");

				//Get service configuration value from DB
				InitializeServiceConfiguration();

				if (IsServiceEnable)
				{
					DataTable dtSMSQueue = new DataTable();
					dtSMSQueue.Columns.Add("Success");
					DataTable dtSMSResponseErrorCodes = new DataTable();
					bool isSourceConnected = false;
					try
					{
						AppendLog("loading SMS queue from source database....");

						///load all pending SMS
						dtSMSQueue = SMSService.GetAllSMSQueueRecords();

						if (dtSMSQueue != null)
						{
							isSourceConnected = true;
							dtSMSQueue.Columns.Add("Success");

							AppendLog(string.Format("Connected and loaded SMS queue.Total Recors Count:{0}", dtSMSQueue.Rows.Count));
						}
						else
						{
							AppendLog("Connected and loaded SMS queue.dtSMSQueue is null");
						}
					}
					catch (Exception ex)
					{
						AppendLog("Failed to connect to source database...." + ex.Message + " " + ex.StackTrace);
					}

					int successCount = 0;


					if (isSourceConnected)
					{
						////////////////////////////////////////////////////////////////////////////////////////////
						SendSMSProcess(dtSMSQueue, dtSMSResponseErrorCodes, defaultSenderName, ref successCount, ref sbLog);
						////////////////////////////////////////////////////////////////////////////////////////////

						////////////////////////////////////////////////////////////////////////////////////////////
						UpdateSMSTracking(dtSMSQueue, ref sbLog);
						////////////////////////////////////////////////////////////////////////////////////////////


						AppendLog(sbLog.ToString());
					}

					AppendLog(string.Format("{0} of {1} SMS Queue updated successfully.", successCount, dtSMSQueue.Rows.Count));
				}
				else
				{
					AppendLog("Service is Disabled.");
				}
			}
			catch (Exception ex)
			{
				//if (!EventLog.SourceExists(EventSource))
				//{
				//    EventLog.CreateEventSource(EventSource, ApplicationEventLog);
				//}
				//EventLog.WriteEntry(EventSource, string.Format(@"Error while service execution. 
				//    Error :{0}, Stack trace : {1} ", ex.Message, ex.StackTrace), EventLogEntryType.Error);

				try
				{
					sbLog.AppendLine(string.Format("Error while Sending SMS.Error:{0}.Exception:{1}", ex.Message, ex.StackTrace.ToString().Trim()));
					AppendLog(sbLog.ToString());
				}
				catch
				{
					///Error while writing log, so no action needs to be done here.
				}
			}
			finally
			{
				isSynchronizing = false;
			}

		}

		public void InitializeServiceConfiguration()
		{
			APIurl = ConfigurationManager.AppSettings["url"].ToString();
			user = ConfigurationManager.AppSettings["user"].ToString();
			pass = ConfigurationManager.AppSettings["pass"].ToString();

			userId = ConfigurationManager.AppSettings["UserID"].ToString();
			if (ConfigurationManager.AppSettings["IsServiceEnable"].ToString() == "1")
			{
				IsServiceEnable = true;
			}
			else
			{
				IsServiceEnable = false;
			}

		}

		private void UpdateSMSTracking(DataTable dt, ref StringBuilder sbLog)
		{
			DataTable copyDataTable;
			copyDataTable = dt.Copy();
			copyDataTable.Columns.Remove("Success");
			SMSService.UpdateSMSTracking(copyDataTable);
			sbLog.AppendLine("<<<Updated main table - SMSTracking>>>");
		}
		private void SendSMSProcess(DataTable dtSMSQueue, DataTable dtSMSResponseErrorCodes, string senderName, ref int successCount, ref StringBuilder sbLog)
		{
			int limit = (batchLimit > 0) ? batchLimit : 10;
			int interval = dtSMSQueue.Rows.Count / limit;

			//if (interval > 0)
			//{
			//	interval = interval + 1;

			//	sbLog.AppendLine("<<<With Thread>>>");
			//	////======================================================
			//	DataTable[] dividedData = DivideData(interval, batchLimit, dtSMSQueue);
			//	////======================================================
			//	StringBuilder[] threadSbLogs = new StringBuilder[interval + 1];

			//	Parallel.For(0, interval, threadCount =>
			//	{
			//		threadSbLogs[threadCount] = new StringBuilder();
			//		SendSMSProcess(dividedData[threadCount], dtSMSResponseErrorCodes, senderName, threadSbLogs[threadCount]);
			//		Thread.Sleep(100);
			//	});

			//	for (int count = 0; count < threadSbLogs.Length; count++)
			//	{
			//		if (threadSbLogs[count] != null)
			//		{
			//			sbLog.Append(threadSbLogs[count].ToString());
			//		}
			//	}

			//	dtSMSQueue.Rows.Clear();
			//	for (int count = 0; count < dividedData.Length; count++)
			//	{
			//		if (dividedData[count] != null && dividedData[count].Rows.Count > 0)
			//		{
			//			dtSMSQueue.Merge(dividedData[count]);
			//		}
			//	}
			//}
			//else
			//{
				sbLog.AppendLine("<<<Without Thread>>>");
				SendSMSProcess(dtSMSQueue, dtSMSResponseErrorCodes, senderName, sbLog);
			//

			DataRow[] drSMSQueue = dtSMSQueue.Select("Success IS NOT NULL AND Success = 1");

			if ((drSMSQueue != null) && (drSMSQueue.Count() > 0))
			{
				// Get sucess count of the image compress.
				successCount = drSMSQueue.Count();
			}
		}
		private DataTable[] DivideData(int interval, int batchLimit, DataTable mainData)
		{
			DataTable[] dividedData = new DataTable[interval + 1];
			for (int count = 0; count <= interval; count++)
			{
				if (count == 0)
				{
					dividedData[count] = new DataTable();
					dividedData[count] = mainData.AsEnumerable().Take(batchLimit).CopyToDataTable();
				}
				else
				{
					int remaining = (mainData.Rows.Count - (count * batchLimit));
					if (remaining > 0)
					{
						if (batchLimit > remaining)
						{
							dividedData[count] = new DataTable();
							dividedData[count] = mainData.AsEnumerable().Skip(count * batchLimit).Take(remaining).CopyToDataTable();
						}
						else
						{
							dividedData[count] = new DataTable();
							dividedData[count] = mainData.AsEnumerable().Skip(count * batchLimit).Take(batchLimit).CopyToDataTable();
						}
					}
				}
			}
			return dividedData;
		}

		private void SendSMSProcess(DataTable dtSMSQueue, DataTable dtSMSResponseErrorCodes, string senderName, StringBuilder sbLog)
		{
			if (dtSMSQueue.Rows.Count > 0)
			{
				for (int index = 0; index < dtSMSQueue.Rows.Count; index++)
				{
					string result = string.Empty;
					DataRow drSMSQueue = dtSMSQueue.Rows[index];

					sbLog.AppendLine(string.Format("SMSId={0}", drSMSQueue["ID"]));

					//===============================================================================================
					result = SendSMSUsingMsegatLinkAPI(drSMSQueue, url: APIurl, user: user, pass: pass, sender: senderName, userID: Guid.Parse(userId), sbLog: sbLog);
					//===============================================================================================

					sbLog.AppendLine(string.Format("Response:{0}", result));


				}
			}
		}
		public static string SendSMSUsingMsegatLinkAPI(DataRow drSMSQueue, string url, string user, string pass,
			string sender, Guid? userID, StringBuilder sbLog)
		{
			// string to, string messageParameter,Guid? contractID,int messageTemplate
			string result = string.Empty;
			int flag = 0, count = 0;
			string data = "";
			Guid? contractID = drSMSQueue.Field<Guid?>("ContractID");
			long? MessageTemlplatedId = drSMSQueue.Field<long?>("SMSType");
			decimal to = drSMSQueue.Field<decimal>("SMSTo");
			string SMSTetMobileNo = ConfigurationManager.AppSettings["SMSTestMobileNo"].ToString();
			if (!string.IsNullOrEmpty(SMSTetMobileNo))
			{
				to = Convert.ToDecimal(SMSTetMobileNo);
			}
			//to = 100010001;

			string messageParameter = SMSService.GetMessageTemplate(MessageTemlplatedId);
			int? TransactionId=drSMSQueue.Field<int?>("TransactionReffId");
			DataTable transactioninfo = SMSService.GetTransactionInfo(TransactionId, MessageTemlplatedId);
			if (contractID != null)
			{
				messageParameter = string.Format(messageParameter, drSMSQueue.Field<string>("BucketStatus"), drSMSQueue.Field<int?>("DPDCount"));
				string replacedString = "";
				


				 replacedString = ReplaceSMSPrefix(contractID, messageParameter);
				if (TransactionId != null)
				{
					replacedString = ReplacetTransactionInfo(transactioninfo, replacedString);
				}
				messageParameter = replacedString.Replace("#","");
		
			}
			try
			{
                do
                {
                    var link = string.Format("{0}?user={1}&pass={2}&to={3}&message={4}&sender={5}", url, user, pass, to, messageParameter, sender);
					count++;
					using (var client = new HttpClient())
					{
						ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
						//client.SecurityProtocol = SecurityProtocolType.Tls12;
						client.Timeout = TimeSpan.FromMinutes(20);
						var uri = new Uri(link);
						var Send = client.GetAsync(uri).ContinueWith(task =>
						{
							if (task.Status == TaskStatus.RanToCompletion)
							{
								var responce = task.Result;
								if (responce.IsSuccessStatusCode)
								{
									flag = 1;
									data = responce.Content.ReadAsStringAsync().Result;
								}
							}

						});
						Send.Wait();
						//flag = 1;
					}
				} while (count <= 5 && flag == 0);
				//var status = InsertIntoSMSTrack(data, userID, messageTemplate);
				//data = "MSG_ID: 232018075 | STATUS: Success | Total: 1 | Cost: 2";
				//string[] authorsList = data.Split(new char[] {'|'});
				//if (authorsList.Length >= 2)
				//{
				//	string[] Status = authorsList[1].Split(new char[] { ':' });
				//	if(Status.Length>)
				//}
				if (data.Contains("Success"))
				{
					drSMSQueue["Success"] = 1;
					drSMSQueue["IsSMSSend"] = 1;
				}
				else
				{
					drSMSQueue["Success"] = 0;
					drSMSQueue["IsSMSSend"] = 0;
				}

				drSMSQueue["SMSStatus"] = data;
				drSMSQueue["SMSText"] = messageParameter;

				drSMSQueue["SMSSendTime"] = DateTime.Now;

			}
			catch (Exception ex)
			{
				data = ex.Message;

				sbLog.AppendLine(string.Format(@"Error while sending/recieving MsegatLink HTTP request/response. 
                            Error :{0}, Stack trace : {1} ", ex.Message, ex.StackTrace));

				drSMSQueue["Success"] = 0;
			}
			return data;
		}

		/// <summary>
		/// Replace Updated method with transaction Info
		/// </summary>
		/// <param name="table"></param>
		/// <param name="Message"></param>
		/// <returns> Updated Message parameter</returns>
		public static string ReplacetTransactionInfo(DataTable table, string Message)
		{
			try
			{
				string[] prefixArray = {"#AccountNumber","#Amount","#DueDt","#BillStatus","#BillCycle","#BillType"};

				//DataTable contractdata = SMSService.GetContractDetailForMessageTemp(ContractID);
			if (table != null && table.Rows.Count > 0)
				{
					foreach (DataRow row in table.Rows)
					{
						foreach (var item in prefixArray)
						{
							string temp = @"\b" + item + @"\b";
							//FirstRow[k] = Regex.Replace(FirstRow[k], temp, item.SimahColName);
							if (item == "#AccountNumber")
							{
								Message = Message.Replace("#AccountNumber", Convert.ToString(row.Field<string>("AccountNumber")));
							}
							if (item == "#Amount")
							{
								Message = Message.Replace("#Amount", Convert.ToString(row.Field<decimal?>("Amount")));
							}
							if (item == "#DueDt")
							{
								Message = Message.Replace("#DueDt", row.Field<System.DateTime?>("DueDt")?.ToString("dd-MM-yyyy"));
							}
							if (item == "#BillStatus")
							{
								Message = Message.Replace("#BillStatus", Convert.ToString(row.Field<string>("BillStatus")));
							}
							if (item == "#BillCycle")
							{
								Message = Message.Replace("#BillCycle", Convert.ToString(row.Field<string>("BillCycle")));
							}
							if (item == "#BillType")
							{
								Message = Message.Replace("#BillType", Convert.ToString(row.Field<string>("BillType")));
							}
						}
					}
				}


			}
			catch (Exception ex)
			{
				var a = ex;
			}
			return Message;
		}
		public static string ReplaceSMSPrefix(Guid? ContractID, string Message)
		{
			try
			{
				string[] prefixArray = {"#NID","#FirstName","#LastName", "#MobileNumber", "#Email", "#OriginatorID", "#MortgageLoanAccountNumber",
		"#OriginalAPR","#ContractTypeID","#OriginationDate","#MaturityDate","#InitialTerm","#OriginalLoanAmount","#CurrentLoanAmount",
		"#RemainingTerm","#CurrentMonthlyInstalment","#TotalPayableAmount","#CurrentArrearsBalance","#TermCostRate","#Outstanding",
		"#LMSRef","#TransactionID"};

				DataTable contractdata = SMSService.GetContractDetailForMessageTemp(ContractID);
				if (contractdata != null && contractdata.Rows.Count > 0)
				{
					foreach (DataRow row in contractdata.Rows)
					{
						foreach (var item in prefixArray)
						{
							string temp = @"\b" + item + @"\b";
							//FirstRow[k] = Regex.Replace(FirstRow[k], temp, item.SimahColName);
							if (item == "#NID")
							{
								Message = Message.Replace("#NID", Convert.ToString(row.Field<decimal?>("NationalID")));
							}
							if (item == "#FirstName")
							{
								Message = Message.Replace("#FirstName", Convert.ToString(row.Field<string>("FirstName")));
							}
							if (item == "#LastName")
							{
								Message = Message.Replace("#LastName", Convert.ToString(row.Field<string>("LastName")));
							}
							if (item == "#MobileNumber")
							{
								Message = Message.Replace("#MobileNumber", Convert.ToString(row.Field<decimal?>("MobileNumber")));
							}
							if (item == "#Email")
							{
								Message = Message.Replace("#Email", Convert.ToString(row.Field<string>("Email")));
							}
							if (item == "#OriginatorID")
							{
								Message = Message.Replace("#OriginatorID", Convert.ToString(row.Field<string>("OriginatorNameInEn")));
							}
							if (item == "#MortgageLoanAccountNumber")
							{
								Message = Message.Replace("#MortgageLoanAccountNumber", Convert.ToString(row.Field<decimal?>("MortgageLoanAccountNumber")));
							}
							if (item == "#OriginalAPR")
							{
								Message = Message.Replace("#OriginalAPR", Convert.ToString(row.Field<string>("OriginalAPR")));
							}
							if (item == "#ContractTypeID")
							{
								Message = Message.Replace("#ContractTypeID", Convert.ToString(row.Field<long?>("ContractTypeID")));
							}
							if (item == "#OriginationDate")
							{
								Message = Message.Replace("#OriginationDate", row.Field<string>("OriginationDate"));
							}
							if (item == "#MaturityDate")
							{
								Message = Message.Replace("#MaturityDate", row.Field<string>("MaturityDate"));
							}
							if (item == "#InitialTerm")
							{
								Message = Message.Replace("#InitialTerm", Convert.ToString(row.Field<decimal?>("InitialTerm")));
							}
							if (item == "#OriginalLoanAmount")
							{
								Message = Message.Replace("#OriginalLoanAmount", Convert.ToString(row.Field<string>("OriginalLoanAmount")));
							}
							if (item == "#CurrentLoanAmount")
							{
								Message = Message.Replace("#CurrentLoanAmount", row.Field<string>("CurrentLoanAmount"));
							}
							if (item == "#RemainingTerm")
							{
								Message = Message.Replace("#RemainingTerm", Convert.ToString(row.Field<decimal?>("RemainingTerm")));
							}
							if (item == "#CurrentMonthlyInstalment")
							{
								Message = Message.Replace("#CurrentMonthlyInstalment", Convert.ToString(row.Field<string>("CurrentMonthlyInstallment")));
							}
							if (item == "#TotalPayableAmount")
							{
								Message = Message.Replace("#TotalPayableAmount", Convert.ToString(row.Field<string>("TotalPayableAmount")));
							}
							if (item == "#CurrentArrearsBalance")
							{
								Message = Message.Replace("#CurrentArrearsBalance", Convert.ToString(row.Field<string>("CurrentArrearsBalance")));
							}
							if (item == "#TermCostRate")
							{
								Message = Message.Replace("#TermCostRate", Convert.ToString(row.Field<string>("TermCostRate")));
							}
							if (item == "#Outstanding")
							{
								Message = Message.Replace("#Outstanding", Convert.ToString(row.Field<string>("Outstanding")));
							}
							if (item == "#LMSRef")
							{
								Message = Message.Replace("#LMSRef", Convert.ToString(row.Field<string>("LMSReferenceNo")));
							}
						}
					}
				}


			}
			catch (Exception ex)
			{
				var a = ex;
			}
			return Message;
		}

	}




}

