﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LMSSMSService
{
    public class SMSService
    {
		// This function write log to LogFile.text when some error occurs. 
		public static void AppendLog(string log)
		{
			FileStream fs = null;
			StreamWriter sw = null;
			string EventSource = "SMS Notification Service";
			string ApplicationEventLog = "Application";
			try
			{
				string logDirectory = ConfigurationManager.AppSettings["LogDirectoryPath"].ToString();
				string logFileFormat = ConfigurationManager.AppSettings["LogFileFormat"].ToString();
				string logFileName = string.Format(logFileFormat, DateTime.Now);
				fs = File.Open(logDirectory + logFileName, FileMode.Append, FileAccess.Write);
				sw = new StreamWriter(fs);
				sw.WriteLine(string.Format("{0} : {1}", DateTime.Now, log));

			}
			catch (Exception ex)
			{
				if (!EventLog.SourceExists(EventSource))
				{
					EventLog.CreateEventSource(EventSource, ApplicationEventLog);
				}
				EventLog.WriteEntry(EventSource, string.Format(@"Error in AppendLog.Could not append the Log.Error :{0}, Stack trace : {1} ", ex.Message, ex.StackTrace), EventLogEntryType.Error);
			}
			finally
			{
				sw.Flush();
				sw.Close();
				fs.Close();
			}

		}
		public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        // This function write Message to log file.
        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static DataTable GetAllSMSQueueRecords()
        {
            DataTable dt = new DataTable();
            try
            {
                Database db = DatabaseFactory.CreateDatabase();
               
                DbCommand dbCommand = db.GetStoredProcCommand("GetSMSTrackingQueue");
                DataSet ds = db.ExecuteDataSet(dbCommand);
                if (ds.Tables.Count >= 0)
                {
                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
            }
            return dt;
        }
        public static DataTable GetContractDetailForMessageTemp(Guid? ContractID)
        {
            DataTable dt = new DataTable();
            try
            {
                Database db = DatabaseFactory.CreateDatabase();

                DbCommand dbCommand = db.GetStoredProcCommand("getContractDetailForMessageTemp");
                db.AddInParameter(dbCommand, "ContractID", DbType.Guid, ContractID); ;
                DataSet ds = db.ExecuteDataSet(dbCommand);
                if (ds.Tables.Count >= 0)
                {
                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
            }
            return dt;
        }
        public static string GetMessageTemplate(long? Id)
        {
            DataTable dt = new DataTable();
            string MessageTemplate = string.Empty;
            try
            {
                Database db = DatabaseFactory.CreateDatabase();

                DbCommand dbCommand = db.GetSqlStringCommand("SELECT * FROM MessageTemplate WHERE MessageTemlplatedId ="+ Id);               
                DataSet ds = db.ExecuteDataSet(dbCommand);
                if (ds.Tables.Count >= 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow row in dt.Rows)
                    {
                        MessageTemplate = row.Field<string>("Template");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
            }
            return MessageTemplate;
        }


        /// <summary>
        /// Bill issue Information
        /// </summary>
        /// <param name="Id"> Bill Transfer reff Id</param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static DataTable GetTransactionInfo(int? Id,long? messageId )
        {
            DataTable dt = new DataTable();
            string MessageTemplate = string.Empty;
            try
            {
                Database db = DatabaseFactory.CreateDatabase();

               // DbCommand dbCommand = db.GetSqlStringCommand("SELECT * FROM BillIssueTransactionInfo WHERE Id =" + Id);
                DbCommand dbCommand = db.GetStoredProcCommand("[getTransactionInfo]");
                db.AddInParameter(dbCommand, "@TransactionId", DbType.Int32, Id);
                db.AddInParameter(dbCommand, "@MessageId", DbType.Int32, messageId);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                if (ds.Tables.Count >= 0)
                {
                    dt = ds.Tables[0];
                    //foreach (DataRow row in dt.Rows)
                    //{
                    //    MessageTemplate = row.Field<string>("Template");
                    //}
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
            }
            return dt;
        }
        public static int UpdateSMSTracking(DataTable dt)
		{
			int ds = 0;
			try
			{
                DataColumnCollection columns = dt.Columns;
                if (columns.Contains("TransactionReffId"))
                {

                    dt.Columns.Remove("TransactionReffId");
                }
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
				DbCommand dbCommand = db.GetStoredProcCommand("UpdateSMSTrackingAfterSMS");
				db.AddInParameter(dbCommand, "@SMSTrackingRec", SqlDbType.Structured, dt);
				 ds = db.ExecuteNonQuery(dbCommand);				
			}
			catch (Exception ex)
			{
				WriteErrorLog(ex);
			}
			return ds;
		}

	}
}
