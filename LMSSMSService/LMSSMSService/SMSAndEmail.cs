﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LMSSMSService
{
    public class SMSAndEmail
    {
        static string[] prefixArray = {"#NID","#FirstName","#LastName", "#MobileNumber", "#Email", "#OriginatorID", "#MortgageLoanAccountNumber",
        "#OriginalAPR","#ContractTypeID","#OriginationDate","#MaturityDate","#InitialTerm","#OriginalLoanAmount","#CurrentLoanAmount",
        "#RemainingTerm","#CurrentMonthlyInstalment","#TotalPayableAmount","#CurrentArrearsBalance","#TermCostRate","#Outstanding",
        "#LMSRef","#TransactionID"};
        

        public string SendMessage(string url, string user, string pass, string to, string messageParameter, string sender, Guid? contractID)
        {
            return SendSms(url, user, pass, to, messageParameter, sender, null, null, 0);

        }
        public static string SendSms(string url, string user, string pass, string to, string messageParameter, string sender, Guid? contractID, Guid? userID, int messageTemplate)
        {
            int flag = 0, count = 0;
            string data = "";

            if (contractID != null)
            {
                //var replacedString = ReplaceSMSPrefix(contractID, messageParameter);
                //messageParameter = replacedString;
            }
            try
            {
                do
                {
                    var link = string.Format("{0}?user={1}&pass={2}&to={3}&message={4}&sender={5}", url, user, pass, to, messageParameter, sender);
                    count++;
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(link);
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                    flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                }
                            }

                        });
                        Send.Wait();
                    }
                } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate);
            }
            catch (Exception ex)
            {
                var a = ex;
            }
            return data;
            //  return messageParameter;

        }
        //public static bool InsertIntoSMSTrack(string status, Guid? userID, int messageTemplate)
        //{
        //    using (var context = new RTOEntities())
        //    {
        //        context.SMSTrackings.Add(
        //          new SMSTracking()
        //          {
        //              SMSID = System.Guid.NewGuid(),
        //              CreatedBy = userID,
        //              SMSStatus = status,
        //              CreatedOn = System.DateTime.Now,
        //              SMSType = messageTemplate

        //          });
        //        context.SaveChanges();
        //    }
        //    return true;
        //}
        //public static string ReplaceSMSPrefix(Guid? ContractID, string Message)
        //{
        //    try
        //    {


        //        using (var context = new RTOEntities())
        //        {
        //            var contractdata = context.getContractDetailForMessageTemp(ContractID).FirstOrDefault();

        //            foreach (var item in prefixArray)
        //            {
        //                string temp = @"\b" + item + @"\b";
        //                //FirstRow[k] = Regex.Replace(FirstRow[k], temp, item.SimahColName);
        //                if (item == "#NID")
        //                {
        //                    Message = Message.Replace("#NID", Convert.ToString(contractdata.NationalID));
        //                }
        //                if (item == "#FirstName")
        //                {
        //                    Message = Message.Replace("#FirstName", Convert.ToString(contractdata.FirstName));
        //                }
        //                if (item == "#LastName")
        //                {
        //                    Message = Message.Replace("#LastName", Convert.ToString(contractdata.LastName));
        //                }
        //                if (item == "#MobileNumber")
        //                {
        //                    Message = Message.Replace("#MobileNumber", Convert.ToString(contractdata.MobileNumber));
        //                }
        //                if (item == "#Email")
        //                {
        //                    Message = Message.Replace("#Email", Convert.ToString(contractdata.Email));
        //                }
        //                if (item == "#OriginatorID")
        //                {
        //                    Message = Message.Replace("#OriginatorID", Convert.ToString(contractdata.OriginatorNameInEn));
        //                }
        //                if (item == "#MortgageLoanAccountNumber")
        //                {
        //                    Message = Message.Replace("#MortgageLoanAccountNumber", Convert.ToString(contractdata.MortgageLoanAccountNumber));
        //                }
        //                if (item == "#OriginalAPR")
        //                {
        //                    Message = Message.Replace("#OriginalAPR", Convert.ToString(contractdata.OriginalAPR));
        //                }
        //                if (item == "#ContractTypeID")
        //                {
        //                    Message = Message.Replace("#ContractTypeID", Convert.ToString(contractdata.ContractTypeID));
        //                }
        //                if (item == "#OriginationDate")
        //                {
        //                    Message = Message.Replace("#OriginationDate", contractdata.OriginationDate);
        //                }
        //                if (item == "#MaturityDate")
        //                {
        //                    Message = Message.Replace("#MaturityDate", contractdata.MaturityDate);
        //                }
        //                if (item == "#InitialTerm")
        //                {
        //                    Message = Message.Replace("#InitialTerm", Convert.ToString(contractdata.InitialTerm));
        //                }
        //                if (item == "#OriginalLoanAmount")
        //                {
        //                    Message = Message.Replace("#OriginalLoanAmount", Convert.ToString(contractdata.OriginalLoanAmount));
        //                }
        //                if (item == "#CurrentLoanAmount")
        //                {
        //                    Message = Message.Replace("#CurrentLoanAmount", contractdata.CurrentLoanAmount);
        //                }
        //                if (item == "#RemainingTerm")
        //                {
        //                    Message = Message.Replace("#RemainingTerm", Convert.ToString(contractdata.RemainingTerm));
        //                }
        //                if (item == "#CurrentMonthlyInstalment")
        //                {
        //                    Message = Message.Replace("#CurrentMonthlyInstalment", Convert.ToString(contractdata.CurrentMonthlyInstallment));
        //                }
        //                if (item == "#TotalPayableAmount")
        //                {
        //                    Message = Message.Replace("#TotalPayableAmount", Convert.ToString(contractdata.TotalPayableAmount));
        //                }
        //                if (item == "#CurrentArrearsBalance")
        //                {
        //                    Message = Message.Replace("#CurrentArrearsBalance", Convert.ToString(contractdata.CurrentArrearsBalance));
        //                }
        //                if (item == "#TermCostRate")
        //                {
        //                    Message = Message.Replace("#TermCostRate", Convert.ToString(contractdata.TermCostRate));
        //                }
        //                if (item == "#Outstanding")
        //                {
        //                    Message = Message.Replace("#Outstanding", Convert.ToString(contractdata.Outstanding));
        //                }
        //                if (item == "#LMSRef")
        //                {
        //                    Message = Message.Replace("#LMSRef", Convert.ToString(contractdata.LMSReferenceNo));
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var a = ex;
        //    }
        //    return Message;
        //}
    }
}
